const onesNames = ["", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
const teensNames = ["", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
const tensNames = ["", "ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"]
const hundredsNames = ["", "one hundred", "two hundred", "three hundred", "four hundred", "five hundred", "six hundred", "seven hundred", "eight hundred", "nine hundred"]

function textToPage(text) {
    const newDiv = document.createElement('div')
    newDiv.textContent = text
    list.appendChild(newDiv)
}

function numbersToWords () {
    for (let hundred = 0; hundred < hundredsNames.length; hundred++) {
        for (let ten = 0; ten < tensNames.length; ten++) {
            for (let one = 0; one < onesNames.length; one++) {
                if (ten == 1 && one !== 0) {
                    textToPage(hundredsNames[hundred] + ' ' + teensNames[one])
                }
                else {
                    textToPage(hundredsNames[hundred] + ' ' + tensNames[ten] + ' ' + onesNames[one])
                }
            }
        }
    }
    textToPage('one thousand')  
}
numbersToWords()